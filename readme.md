Brown Rabbit
============


Contents
--------

I use in this projects with these technologies
* git
* html5
* css & css 3
* js/jQuery
* Bootstrap 4

Requirements
--------

* Fork this repository
* Full responsive
* Use script Highlight for search and hightlight anythis that you type
* Slider show random Image every time the page is refreshed 
* Click on more button in article to show rest of the content
* Write jquery for pagination in article
* Create a pull request to this repository