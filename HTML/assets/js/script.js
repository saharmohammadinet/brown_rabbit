$( document ).ready(function() {
//    search
    var elems = document.querySelectorAll('.text');
    $('.search').textHighlight(elems, true);
//     slider
if( $(".slider").length > 0 ) {
    randomImage();
    function randomImage() {
        var images = new Array()
        images[1] = "slider1.jpg"
        images[2] = "slider2.jpg"
        images[3] = "slider4.jpg"
        images[4] = "slider5.jpg"
        images[5] = "slider1.jpg"
        images[6] = "slider2.jpg"
        images[7] = "slider4.jpg"
        images[8] = "slider5.jpg"
        for (i = 0; i < 1; i++) {
            var ranPic = Math.floor(Math.random() * images.length)
            if (ranPic == 0)
                ranPic = 1
            // window.location.replace("https://iranserver.com/hafez/"+images[ranPic]);
            $(".slider img").attr("src","assets/images/"+images[ranPic]);
        }
    }
}

//     paging
    var article = $(".article article").length;
    var num =parseInt( article/4 );
    var i =1;
    while (i <= num) {
        if(i==1){
            $(".pagination .page-number").append("<a href='#articles' class='active'>" + i + "</a>");
        }else {
            $(".pagination .page-number").append("<a href='#articles'>" + i + "</a>");
        }
        i++;
    }
    $(".article article:nth-child(n+1):nth-child(-n+4)").addClass("d-block");
    $(".pagination .prev").addClass("d-none");
    $(".article article").addClass("d-none");
    $(".pagination .page-number a").on('click',function(){
        $(".pagination .page-number a").removeClass('active');
        $(this).addClass('active');
        var active = $(".pagination .page-number a.active").html();

        if(active==2){
            $(".article article:nth-child(n+1):nth-child(-n+4)").removeClass("d-block");
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+5):nth-child(-n+10)").addClass("d-block");
            $(".pagination .next").removeClass("d-none");
            $(".pagination .prev").removeClass("d-none");
        } else if(active==3){
            $(".article article:nth-child(n+1):nth-child(-n+10)").removeClass("d-block");
            $(".article article:nth-child(n+11):nth-child(-n+15)").addClass("d-block");
                $(".pagination .next").addClass("d-none");
                $(".pagination .prev").removeClass("d-none");
        }else {
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+1):nth-child(-n+4)").addClass("d-block");
            $(".pagination .prev").addClass("d-none");
            $(".pagination .next").removeClass("d-none");
        }
    });
    $(".pagination .next").on("click",function(){
        var active2 = $(".pagination .page-number a.active").html();
        active2 = parseInt(active2)+1;
        if(active2==2) {
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:nth-child(" + active2 + "n)").addClass("active");
            $(".article article:nth-child(n+1):nth-child(-n+4)").removeClass("d-block");
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+5):nth-child(-n+10)").addClass("d-block");
            $(".pagination .next").removeClass("d-none");
            $(".pagination .prev").removeClass("d-none");
        }else if(active2==3){
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:nth-child(" + active2 + "n)").addClass("active");
            $(".article article:nth-child(n+1):nth-child(-n+10)").removeClass("d-block");
            $(".article article:nth-child(n+11):nth-child(-n+15)").addClass("d-block");
            $(".pagination .next").addClass("d-none");
            $(".pagination .prev").removeClass("d-none");
        } else {
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:first-child").addClass("active")
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+1):nth-child(-n+4)").addClass("d-block");
            $(".pagination .prev").addClass("d-none");
            $(".pagination .next").removeClass("d-none");
        }


    })


    $(".pagination .prev").on("click",function(){
        var active2 = $(".pagination .page-number a.active").html();
        active2 = parseInt(active2)-1;
        if(active2==2) {
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:nth-child(" + active2 + "n)").addClass("active");
            $(".article article:nth-child(n+1):nth-child(-n+4)").removeClass("d-block");
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+5):nth-child(-n+10)").addClass("d-block");
            $(".pagination .next").removeClass("d-none");
            $(".pagination .prev").removeClass("d-none");
        }else if(active2==3){
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:nth-child(" + active2 + "n)").addClass("active");
            $(".article article:nth-child(n+1):nth-child(-n+10)").removeClass("d-block");
            $(".article article:nth-child(n+11):nth-child(-n+15)").addClass("d-block");
            $(".pagination .next").addClass("d-none");
            $(".pagination .prev").removeClass("d-none");
        } else {
            $(".pagination .page-number a.active").removeClass("active");
            $(".pagination .page-number a:first-child").addClass("active")
            $(".article article:nth-child(n+6):nth-child(-n+15)").removeClass("d-block");
            $(".article article:nth-child(n+1):nth-child(-n+4)").addClass("d-block");
            $(".pagination .prev").addClass("d-none");
            $(".pagination .next").removeClass("d-none");
        }


    })
//     more link article
    $(".more-article").click(function(){
        event.preventDefault();
        $(this).prev().removeClass("text-truncate");
    });


});